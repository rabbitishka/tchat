<?php

  function chargerClasse ($classe){
    if (file_exists('../classes/'.$classe . '.php')) {
      require '../classes/'.$classe . '.php'; // On inclue la classe correspondante au paramètre passé
    } else if(file_exists('../classes/repository/'.$classe . '.php')){
      require '../classes/repository/'.$classe . '.php';
    } else {
      exit("Le fichier $classe.php n'existe ni dans classe ni dans repository.");
    }

  }

spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée

session_start();