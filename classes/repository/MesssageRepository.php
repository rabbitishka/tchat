<?php

class MessageRepository {
    private $bdd;

    public function __construct(){
        $this->bdd = new BDD();
        $this->bdd = $this->bdd->getPDO();
      }
      
      public function createMessage(array $values) {
        try {
          $sql = "INSERT INTO messages (content, msg_time) VALUES (:content, :msg_time)";
          $stmt= $this->bdd->prepare($sql);
          $stmt->execute([':content' => $values['content'],
                          ':msg_time' => $values['msg_time'],
                        //   ':email' => $values['email']
                        ]);
        } catch(PDOException $error) {
          echo $error->getMessage();
        }
        
      }


      public function getAllMessages(){
        $sql = 'SELECT * FROM DB_TABLE_MESSAGES';
        $query =  $this->bdd->query($sql);
        $result = $query->fetchAll(PDO::FETCH_OBJ);
    
        return $result;
      }

    }

?>