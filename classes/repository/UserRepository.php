<?php

class UserRepository {
    private $bdd;

    public function __construct(){
        $this->bdd = new BDD();
        $this->bdd = $this->bdd->getPDO();
      }

    public function createUser(array $values) {
      try {
        $sql = "INSERT INTO users (nickname, password, email) VALUES (:nickname, :password, :email)";
        $stmt= $this->bdd->prepare($sql);
        $stmt->execute([':nickname' => $values['nickname'],
                        ':password' => $values['password'],
                        ':email' => $values['email']]);
      } catch(PDOException $error) {
        echo $error->getMessage();
      }
      
    }

    public function getAllUsers(){
      $sql = 'SELECT * FROM DB_TABLE_USERS';
      $query =  $this->bdd->query($sql);
      $result = $query->fetchAll(PDO::FETCH_OBJ);
  
      return $result;
    }
}

// var_dump($bdd);
?>