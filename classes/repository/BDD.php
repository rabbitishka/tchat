<?php 

class BDD {
    const DB_HOST = "localhost";
    const DB_BASE = "tchat";
    const DB_USER = "tchat";
    const DB_PASSWORD = "tchat";
    const DB_TABLE_USERS = "users";
    const DB_TABLE_MESSAGES = "	messages";
    const DB_TABLE_LECTURE = "	lecture";
  
    private $_PDO;
  
    public function __construct(){
      $this->connectBDD();
    }

  
  private function connectBDD(){
    try {
      $this->_PDO = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      echo "Connectè à " .self::DB_BASE. " au " .self::DB_HOST. " avec succès.";
    } catch (PDOException $e) {
      die("Erreur de connexion : " . $e->getMessage());
    }
  }

  public function getPDO(){
    return $this->_PDO;
  }

  public function closeBDD() {
    $this->_PDO = null;
}

}

$BDD = new BDD;
var_dump($BDD);
$PDO = $BDD->getPDO();
var_dump($PDO);
$BDD->closeBDD();

var_dump($BDD);