<?php
require "../init.php";
// require '../classes/repository/BDD.php';
// session_start();
$bdd = new BDD();


if (isset($_SESSION['connect']) && $_SESSION['connect'] === TRUE) {
    header("Location:chat.php");
    exit();
}

$_SESSION['connect'] = FALSE;
$error_connection = "";

if (isset($_POST) && !empty($_POST['nickname']) && !empty($_POST['password'])) {
    require '../classes/repository/BDD.php';

    $nickname = $_POST["nickname"];
    $password = $_POST["password"];
    // if($password && $nickname) { восстановить

        // sql chercher password lié au nickname
        // $sql = "SELECT * FROM users 
        //         WHERE password = nickname";

        // $sql = "SELECT * FROM users 
        //         WHERE nickname = '".$nickname."' 
        //         AND password = '".$password."';";
        $sql = 'SELECT * FROM users WHERE nickname = :nickname;';
        // $stmt = $bdd->prepare("SELECT * FROM users WHERE nickname = ?");
        // $requete = $bdd->prepare($sql);
        // $requete->execute([$nickname]);
        // $nickname = $stmt->fetch();

        if(password_verify($password, $nickname['password'])) {
            $_SESSION['connect'] = TRUE;
            header("Location:chat.php");
            exit();
        } else {
            $error_connection = "ERROR";
        } 
    } else {
        $error_connection = "manquant";
    }
// }


?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
    <div class="main formulaire">
        <div class="connexion">
            <h3>Connexion</h3>

            <?php
            if ($error_connection == "ERROR") { ?>
                <div class="error">
                    <p>Login ou mot de passe incorrect</p>
                </div>
            <?php } ?>

            <form action="#" method="POST">
                Nom d'utilisateur: <br>
                <input type="text" name="nickname" id="nickname" required> <br>
                Mot de passe : <br>
                <input type="password" name="password" id="password" required> <br> <br>
                <input type="submit" id="submit" value="Se connecter">
                <br>
                <p class="regtext">Pas inscrites ? <a href="register.php">Inscription</a></p>
            </form>
        </div>
    </div>
</body>
</html>