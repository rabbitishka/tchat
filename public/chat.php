<?php
require "../init.php";

$bdd = new BDD();

if(!isset($_SESSION["connect"]) && !isset($_SESSION['nickname'])){
    header("Location: login.php");
    exit();
}

// $repomsg = new MessageRepository();
// $repomsg -> createMessage($values);

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Notre petit chat</title>
</head>
<body>
    <form action="" method="POST">
        <textarea name="message" class="yourmessage" id="message" cols="30" rows="10" placeholder="Ton message là"></textarea>
        <br><br>
        <input type="submit" value="Envoyer">
    </form>
    <table id="allmessages">
        <?php foreach ($bdd->query($query) as $message) { ?>
            <tr>
                <td><?php echo $message['nickname']; ?></td>
                <td><?php echo $message['message']; ?></td>
            </tr>
        <?php } ?>
    </table>
    <br>
    <p><a href="logout.php">Se deconnecter</a></p>
</body>
</html>