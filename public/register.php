<?php
require "../init.php";
$email_valid = "";

function email_valid($string) {
    return filter_var($string, FILTER_VALIDATE_EMAIL);
}

if(isset($_POST["nickname"]) && isset($_POST["password"]) && isset($_POST["email"])){
    $hash_pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
    // var_dump($pass);
    $values= [
        'nickname' => $_POST['nickname'],
        'password' => $hash_pass,
        'email' => $_POST['email'],
    ];

    $repo = new UserRepository();
    $repo -> createUser($values);
    // echo 'Vous êtes inscrits';
    header("Location:login.php");
    exit();
    
    print_r($_POST);

    // $_SESSION['register'] = FALSE;

    // if (isset($_SESSION['register']) && $_SESSION['register'] === TRUE) {
    //     echo 'Voes êtes inscrits';
    //     header("Location:login.php");
    //     exit();
    // }
}




?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
</head>
<body>
    <div class="main formulaire">
        <div class="inscription">
            <h3>Inscription</h3>
            <form action="#" method="POST">
                Nom d'utilisateur: <br>
                <input type="text" name="nickname" id="nickname" required> <br>
                Email : <br>
                <input type="email" name="email" id="email" required> <br>

                <?php
                    if ($email_valid == "invalid") { ?>
                    <div class ="error">
                        <p>Adrresse mail invalide</p>
                    </div>
                <?php } ?>


                Mot de passe : <br>
                <input type="password" name="password" id="password" required><br><br>
                <input type="submit" id="submit" value="S'inscrire">
                <br>
                <p class="regtext">Vous avez deja un compte ? <a href="login.php">Connexion</a></p>
            </form>
        </div>
    </div>
</body>
</html>